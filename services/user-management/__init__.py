from flask import jsonify, abort
from models import DeclarativeBase
from nameko.rpc import rpc
from nameko_sqlalchemy import DatabaseSession


class UserManagement:
    name = "user_management"

    db = DatabaseSession(DeclarativeBase)

    @rpc
    def get(self, id=None):
        if not id:
            users = self.db.query(User).all()
            res = {}
            for usr in users:
                res[usr.id] = {
                    'username': usr.username,
                    'email': usr.email
                }
        else:
            user = self.db.query(User).get(id)
            if not user:
                abort(404)
            res = {
                'username': user.username,
                'email': user.email
            }
        return jsonify(res)

    @rpc
    def post(self, username, email):
        new_user = User(username, email)
        self.db.add(new_user)
        self.db.commit()
        return jsonify({
            new_user.id: {
                'username': new_user.username,
                'email': new_user.email
            }
        })

    @rpc
    def put(self, id, username, email, password):
        user = self.db.query(User).get(id)
        user.username = username
        user.email = email
        user.set_password(password)
        self.db.commit()
        return jsonify({
            user.id: {
                'username': user.username,
                'email': user.email
            }
        })

    @rpc
    def delete(self, id):
        user = self.db.query(User).get(id)
        self.db.delete(user)
        self.db.commit()
        return jsonify({
            'message': 'delete user {} successfully'.format(user.username)
        })
